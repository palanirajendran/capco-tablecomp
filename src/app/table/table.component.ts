import { Component, OnInit, AfterViewChecked,Input, Output,Renderer, ViewChild, AfterContentChecked, AfterViewInit, ElementRef } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { WindowRef } from './WindowRef';

import { HttpErrorResponse } from '@angular/common/http';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'tc-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers:[ WindowRef ]
})
//  
export class TableComponent implements OnInit, AfterViewInit, AfterContentChecked { 
  @ViewChild('tblbodyData') nameInput:ElementRef;
  
  Element: any;
  public firstText: string = "«";
  public lastText: string = "»";
  public previousText: string = "‹";
  public nextText: string = "›";
  public pgsize:number=10;
  public displaypageFooter:boolean = true;
  public strpgsize:string;
  public pgstart:number =1;
  public pgend:number=10;
  public boundaryLinks:boolean = true;
  public directionLinks:boolean = true;
  public totalrecords:number=0;
  public totalPages:number=0;
  public pages:Array<any>;;
  public rotate:boolean = false;
  public adjacents:number = 2;
  public pageNumber:number = 1;
  public maxPageOptionsDisplay: number = 27;
  public winobj:any;
  public formdata;
  constructor(private httpService: HttpClient, private renderer: Renderer, private winref: WindowRef) {
    this.pgstart=0;
    this.pgsize=10;
    this.pgend=this.pgsize;
    this.strpgsize=this.pgsize.toString();
    this.winobj=winref.nativeWindow;
  }
  arrEmployeesObject: object [];
  arrEmployees: string [];
  arrProperties:string []=new Array();
  strFirstRow:string='';
  
  ngAfterViewInit() {
    

  }
  ngAfterContentChecked() {
    
}

Goto(val): void
{
  this.pageNumber=val;
  console.log(val)
  this.calculateTotalPages();
  this.pages = this.getPages(this.pageNumber, this.totalPages);
  this.pgstart=((val-1)*this.pgsize);
  this.pgend=this.pgstart+this.pgsize;
};

calculateTotalPages() :void{
  var totalPages = this.pgsize < 1 ? 1 : Math.ceil(this.totalrecords / this.pgsize);
  this.totalPages = Math.max(totalPages || 0, 1);
  this.displaypageFooter=this.totalPages <= 1 ? false : true;
  this.strFirstRow="";
  console.log("initial total pages - "+this.totalPages);
}

Reload(keycod): void
{
       if(keycod==13)
        {
          if( this.strpgsize!="0")
          {
            this.pgsize=parseInt(this.strpgsize);
            this.pageNumber=1;
            this.calculateTotalPages();
            this.pages = this.getPages(this.pageNumber, this.totalPages);
            this.pgstart=0;
            this.pgend=this.pgsize;
          }
        }
        else if(!(keycod >= 48 && keycod <= 57) || this.strpgsize=="0")
        {
          this.strpgsize=this.pgsize.toString();
        }
     
  }

  makePage(number: number, text:string, isActive:boolean) : any {
    return {
      number: number,
      text: text,
      active: isActive
    };
  };

  noPreviousBtn() : boolean {
    return this.pageNumber === 1;
  }
  
  noNextBtn() : boolean {
    return this.pageNumber === this.totalPages;
  }

  getPgText(key:string) : string {
    return this[key + 'Text'] || this[key + 'Text'];
  }
  // method to get pages array
  getPages(currentPage, totalPages) {
    var pages = [];

    // Default page limits
    var startPage: number = 1, endPage: number = totalPages;
    var isMaxPageArraySized: boolean = this.maxPageOptionsDisplay < totalPages;

    var calcMaxPageArraySized:number = isMaxPageArraySized ? this.maxPageOptionsDisplay : 0;

    // If we want to limit the maxSize within the constraint of the adjacents, we can do so like this.
    // This adjusts the maxSize based on current page and current page and whether the front-end adjacents are added.
    if (isMaxPageArraySized && !this.rotate && this.adjacents > 0 && currentPage >= (calcMaxPageArraySized - 1) && totalPages >= (calcMaxPageArraySized + (this.adjacents * 2))) {
      calcMaxPageArraySized = this.maxPageOptionsDisplay - this.adjacents;
    }

    // Adjust max size if we are going to add the adjacents
    if (isMaxPageArraySized && !this.rotate && this.adjacents > 0) {
      var tempStartPage = ((Math.ceil(currentPage / calcMaxPageArraySized) - 1) * calcMaxPageArraySized) + 1;
      var tempEndPage = Math.min(tempStartPage + calcMaxPageArraySized - 1, totalPages);

      if (tempEndPage < totalPages) {
        if (totalPages - this.adjacents > currentPage) { // && currentPage > adjacents) {
          calcMaxPageArraySized = calcMaxPageArraySized - this.adjacents;
        }
      }
    }

    // recompute if maxPageSize
    if (isMaxPageArraySized) {
      if (this.rotate) {
        // Current page is displayed in the middle of the visible ones
        startPage = Math.max(currentPage - Math.floor(calcMaxPageArraySized / 2), 1);
        endPage = startPage + calcMaxPageArraySized - 1;

        // Adjust if limit is exceeded
        if (endPage > totalPages) {
          endPage = totalPages;
          startPage = endPage - calcMaxPageArraySized + 1;
        }
      } else {
        // Visible pages are paginated with maxSize
        startPage = ((Math.ceil(currentPage / calcMaxPageArraySized) - 1) * calcMaxPageArraySized) + 1;

        // Adjust last page if limit is exceeded
        endPage = Math.min(startPage + calcMaxPageArraySized - 1, totalPages);
      }
    }

    // Add page number links
    for (var num = startPage; num <= endPage; num++) {
        var page = this.makePage(num, num.toString(), num === currentPage);
        pages.push(page);
    }

    // Add links to move between page sets
    if (isMaxPageArraySized && !this.rotate) {
      if (startPage > 1) {
        var previousPageSet = this.makePage(startPage - 1, '...', false);
        pages.unshift(previousPageSet);
        if (this.adjacents > 0) {
          if (totalPages >= this.maxPageOptionsDisplay + (this.adjacents * 2)) {
            pages.unshift(this.makePage(2, '2', false));
            pages.unshift(this.makePage(1, '1', false));
          }
        }
      }

      if (endPage < totalPages) {
        var nextPageSet = this.makePage(endPage + 1, '...', false);
        var addedNextPageSet = false;
        if (this.adjacents > 0) {
          if (totalPages - this.adjacents > currentPage) { // && currentPage > adjacents) {
            var removedLast = false;
            addedNextPageSet = true;
            if (pages && pages.length > 1 && pages[pages.length - 1].number == totalPages - 1) {
              pages.splice(pages.length - 1, 1);
              removedLast = true;
            }
            pages.push(nextPageSet);
            if (removedLast || pages[pages.length - 1].number < totalPages - 2 || pages[pages.length - 2].number < totalPages - 2) {
              pages.push(this.makePage(totalPages - 1, (totalPages - 1).toString(), false));
            }

            pages.push(this.makePage(totalPages, (totalPages).toString(), false));
          }
        }

        if (!addedNextPageSet) {
          pages.push(nextPageSet);
        }
      }
    }
console.log("initial - "+pages.length);
    return pages;
  }

  onClickSubmit(id,status) {
    let headers = new HttpHeaders({['Content-Type']: 'application/json'});
//headers.append('Content-Type', 'application/x-www-form-urlencoded');

//let options = new RequestOptions({ headers: headers });

var body = "id=" + id + "&status=" + status;
console.log (body);

    this.httpService.post('./submit',JSON.stringify(body),{headers}).subscribe(data => {
      console.log(data.toString());
      //alert(data.toString());
    },
    (err: HttpErrorResponse) => {
      console.error (err.message);
    }
  );

  }

// method which will set the width of thead cells with tbody cells value.
  setColumnWidths(): void
  {
    var lastcolumnwidth=0; var lastcolumnindx=this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th').length-1;
    for(var i=0;i<this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th').length;i++)
    {
      var ic=this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th')[i].offsetWidth;
    if(this.winobj.document.getElementById("dvcpbody").scrollHeight>this.winobj.document.getElementById("table3").offsetHeight && i==lastcolumnindx)
    {
      lastcolumnwidth=ic//+250;
      this.winobj.document.getElementById('fixedheaderrow').getElementsByTagName('tr')[0].getElementsByTagName('th')[i].style.width=(lastcolumnwidth)+"px";
      this.winobj.document.getElementById('tbodyelem').getElementsByTagName('tr')[0].getElementsByTagName('td')[i].style.width=(lastcolumnwidth-20)+"px";
    console.log('calling - '+lastcolumnwidth)
    }
    else{
      this.winobj.document.getElementById('fixedheaderrow').getElementsByTagName('tr')[0].getElementsByTagName('th')[i].style.width=(ic)+"px";
     }
      
    }
    console.log(this.winobj.document.getElementById("dvcpbody").scrollHeight);
    console.log(this.winobj.document.getElementById("table3").clientWidth);
    console.log(this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th').length-1);
    console.log(this.winobj.document.getElementById('fixedheaderrow').getElementsByTagName('tr')[0].getElementsByTagName('th')[this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th').length-1].innerText);


    var ua = this.winobj.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var msedg=ua.indexOf("Edge/");
    if (msie > 0 || msedg > 0) // If Internet Explorer, return version number
    {
      this.winobj.document.getElementById("dvcpfoot").style.bottom="40px";
      this.winobj.document.getElementById("dvcpfoot").style.width=(this.winobj.document.getElementById("table3").clientWidth-11)+"px";
    }
    else  // If another browser, return 0
    {
      this.winobj.document.getElementById("dvcpfoot").style.bottom="53px";
      this.winobj.document.getElementById("dvcpfoot").style.width=(this.winobj.document.getElementById("table3").clientWidth-11)+"px";;
  
    }

    /*
    if(this.winobj.document.getElementById("dvcpbody").scrollHeight>this.winobj.document.getElementById("table3").offsetHeight)
    {
      //var lastcolumnlength=this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th')[this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th').length-1].offsetWidth+50;
      console.log(lastcolumnwidth);
      this.winobj.document.getElementById('fixedheaderrow').getElementsByTagName('tr')[0].getElementsByTagName('th')[this.winobj.document.getElementById('tblbodyData').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th').length-1].style.width=(lastcolumnwidth)+"px";
    }
    */
    
    
  };

// updating the thead td widths to match with the tbody td's
  callUpdate():void{
    
   if(this.winobj.document.getElementById('tblbodyData').getElementsByTagName('tbody')[0].getElementsByTagName('tr').length>0){
      if(this.strFirstRow=='')
        {this.strFirstRow="1";this.setColumnWidths();}
   } ;
  }

  ngOnInit() {
    // get http call to retreive data from sample data json file
    this.httpService.get('./assets/sample_data.json').subscribe(
      data => {
        var n:number = 0;
        this.arrEmployeesObject = data as object [];
        this.arrEmployees=data as string [] ;
        this.totalrecords=this.arrEmployees.length;
        this.calculateTotalPages();
        this.pages = this.getPages(this.pageNumber, this.totalPages);
        for(var property in this.arrEmployeesObject[0])
        {
          this.arrProperties[n++]=property;
        }
        
      
      },
      (err: HttpErrorResponse) => {
        console.error (err.message);
      }
    );
    
  }

}
